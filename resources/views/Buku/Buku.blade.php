@extends('layout.main')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Data Buku
                </h3>   
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__body">
                <div class="table-responsive">
                    <table 
                        class=" table table-bordered datatable">
                        <thead>
                            <tr>
                                <th class="no-sort" >No</th>
                                <th class="no-sort" >Nama Mahasiswa</th>
                                <th class="no-sort" >NIM</th>
                                <th class="no-sort" >Email</th>
                                <th class="no-sort" >Nomor Telepon</th>
                                <th class="no-sort" >Prodi</th>
                                <th class="no-sort" >Jurusan</th>
                                <th class="no-sort" >Fakultas</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection