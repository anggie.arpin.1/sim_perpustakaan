@extends('layout.main')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">
					Rahajeng Rauh
				</h3>   
			</div>
		</div>
	</div>
</div>
@endsection