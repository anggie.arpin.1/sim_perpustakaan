<?php

use App\Http\Controllers\BukuController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\PeminjamanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Data Peminjaman
Route::get('/Peminjaman', [PeminjamanController::class, 'show'])->name('TampilDataPeminjaman');

//Data Mahasiswa
Route::get('/Mahasiswa', [MahasiswaController::class, 'show'])->name('TampilDataMahasiswa');

//Data Buku
Route::get('/Buku', [BukuController::class, 'show'])->name('TampilDataBuku');